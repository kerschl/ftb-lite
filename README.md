# ![FTB Lite](./logo.png)
This is a customized FTB Lite Modpack which my friends an i played for quite some time. 
(Version 6, based on FTBLite)

- How to install?
Download the NEW FTB Launcher at https://www.feed-the-beast.com.
Then install the FTB Lite 1.4.7 Modpack 1.2.3.
Merge the git/minecraft folder with your local instance of FTB Lite.
Done.

- What have you changed? I added this:
Optifine: /instmods/OptiFine_1.4.6_HD_U_D5.zip
/shaderpacks/*
/texturepacks/*
BackPack: /mods/backpack-1.7.8-1.4.7.zip
BackTools: merged in /instmods/1.4.7-forge1.4.7-6.6.2.534.jar
Chococraft: /mods/Chococraft_2.5.3.zip
Enchantingplus: /mods/enchantingplus_1_12_8.zip
The Twilight Forest: /mods/The-Twilight-Forest-Mod-1.4.6.zip
Smart Moving: /mods/Smart Moving Client for ModLoader or Minecraft Forge.jar (+ lib also merged in jar)
Also Configs for easier use out of the box. /options.txt etc.
